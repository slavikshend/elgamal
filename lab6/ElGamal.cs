﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Reflection.Emit;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Serialization.Metadata;
using System.Threading.Tasks;

namespace lab6
{
    internal class ElGamal
    {   
        public static BigInteger SecretKey;
        public List<BigInteger> Restore(BigInteger a, BigInteger[] bs, BigInteger p)
        {
            List<BigInteger> ms = new List<BigInteger>();
            foreach (BigInteger b in bs)
            {
                ms.Add((BigInteger.ModPow(a, p - 1 - SecretKey, p) * b) % p);
            }
            return ms;
        }
        public (BigInteger a, BigInteger[] bs) SendMessage(BigInteger[] ascii, BigInteger p, BigInteger g, BigInteger y)
        {
            BigInteger k = 0;
            do
            {
                k = GenerateRandomBigInteger();
            }
            while (k <= 1 || k >= p - 1);
            BigInteger a = BigInteger.ModPow(g, k, p);
            BigInteger[] bs = ascii.Select(m => (BigInteger.ModPow(y, k, p) * m) % p).ToArray();
            return (a, bs);
        }
        public (BigInteger p, BigInteger g, BigInteger y) GenerateKeys()
        {
            BigInteger p = 0;
            while (true)
            {
                p = GenerateRandomBigInteger();
                if (MillerRabinTest(p, 3))
                {
                    break;
                }
            }
            BigInteger g = GenerateRandomGenerator(p);
            do
            {
                Console.Write($"Введіть 1 < x < {p}: ");
                SecretKey = BigInteger.Parse(Console.ReadLine());
            }
            while (SecretKey <= 1 || SecretKey >= p - 1);
            BigInteger y = BigInteger.ModPow(g, SecretKey, p);
            return (p, g, y);
        }
        public bool MillerRabinTest(BigInteger n, int k)
        {
            if (n == 2 || n == 3)
                return true;
            if (n < 2 || n % 2 == 0)
                return false;
            BigInteger t = n - 1;
            int s = 0;
            while (t % 2 == 0)
            {
                t /= 2;
                s += 1;
            }
            for (int i = 0; i < k; i++)
            {
                RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
                byte[] _a = new byte[n.ToByteArray().LongLength];
                BigInteger a;
                do
                {
                    rng.GetBytes(_a);
                    a = new BigInteger(_a);
                }
                while (a < 2 || a >= n - 2);
                BigInteger x = BigInteger.ModPow(a, t, n);
                if (x == 1 || x == n - 1)
                    continue;
                for (int r = 1; r < s; r++)
                {
                    x = BigInteger.ModPow(x, 2, n);
                    if (x == 1)
                        return false;

                    if (x == n - 1)
                        break;
                }
                if (x != n - 1)
                    return false;
            }
            return true;
        }
        private BigInteger GenerateRandomGenerator(BigInteger p)
        {
            BigInteger g;
            do
            {
                g = GenerateRandomBigInteger();
            } while (g <= 1 || g >= p - 1 || BigInteger.ModPow(g, p-1, p) != 1);

            return g;
        }
        static BigInteger GenerateRandomBigInteger()
        {
            BigInteger randomBigInt = 0;
            do
            {
                byte[] buffer = new byte[128];
                new Random().NextBytes(buffer);
                randomBigInt = new BigInteger(buffer);
            }
            while (randomBigInt < 1000000000);
            return randomBigInt;
        }
    }
}
