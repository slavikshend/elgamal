﻿using lab6;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.OutputEncoding = Encoding.Unicode;
        Console.InputEncoding = Encoding.Unicode;
        ElGamal elGamal = new ElGamal();
        Console.Write("Введіть ПІБ: ");
        BigInteger[] Ascii = Console.ReadLine().Select(x => (BigInteger) x).ToArray();
        (var p,var g,var y) = elGamal.GenerateKeys();
        Console.WriteLine($"Передається p: {p}");
        Console.WriteLine($"Передається g: {g}");
        Console.WriteLine($"Передається y: {y}");
        (var a, var b) = elGamal.SendMessage(Ascii, p, g, y);
        Console.WriteLine($"Шифротекст a|b: {a}|{string.Join(", ", b.Select(x => x.ToString()).ToArray())}");
        List<BigInteger> deciphered = elGamal.Restore(a, b, p);
        string message = new string(deciphered.Select(x => (char)x).ToArray());
        Console.WriteLine($"Розшифрований текст: {message}");
    }
}